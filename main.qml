import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12

ApplicationWindow {
    width: 680/1.25
    height: 680/1.25+16
    id: window
    visible: true
    color : "#00000000"
    title: "Share"
    flags: Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.WA_TranslucentBackground
    Material.theme: Material.System
    Material.accent: systemPalette.highlight
    Component.onCompleted: {
        x = Screen.width / 2 - width / 2
        y = Screen.height / 2 - height / 2
    }
    SystemPalette { id: systemPalette }
    Pane {
        anchors.fill:parent
        anchors.leftMargin: 60
        anchors.rightMargin: 60
        anchors.topMargin: 60
        anchors.bottomMargin: 60
        Material.elevation: 24
        padding: 0
        Material.background: "#00000000"
        Rectangle {
            anchors.fill:parent
            radius:4
            color: systemPalette.window
        }
        ToolBar {
            id: topToolBar
            Material.background: Material.background
            Material.elevation:0        
            width:parent.width
            height: 46
            Text {
                anchors.leftMargin: 24
                anchors.left: parent.left
                color: systemPalette.text
                font.pixelSize: 20
                anchors.bottom: parent.bottom
                text:"Share..."
            }
        }
        Item {
            anchors.leftMargin:24
            anchors.topMargin:12
            anchors.left:parent.left
            anchors.right:parent.right
            anchors.rightMargin:24//32
            anchors.top: topToolBar.bottom; 
            anchors.bottom: bottomToolBar.top; 
            width: window.width
            height: parent.height

            Text{
                text: description
                anchors.left: parent.left
                anchors.right: parent.right
                maximumLineCount: 2
                elide: Text.ElideRight
                font.pixelSize: 16
                color: systemPalette.text
                opacity: 0.87
                wrapMode: Text.WordWrap
            }
            Grid{
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 24
                anchors.horizontalCenter: parent.horizontalCenter
                columns: 3
                spacing: 2
                Repeater{
                    model: ListModel{
                        ListElement { name: "Share to Twitter"; image: "Twitter_Social_Icon_Circle_White.svg"; colorize: "#1DA1F2"; uri: "https://twitter.com/intent/tweet?text=" }
                        ListElement { name: "New Reddit Post"; image: "Reddit_Mark_OnDark.svg"; colorize: "#FF4500"; uri: "https://www.reddit.com/submit?title=" }
                        ListElement { name: "Google search"; image: "Google_G_Logo.svg"; colorize: "#4285F4"; uri: "https://www.google.com/search?q=" }
                        ListElement { name: "Search Wikipedia"; image: "Wikipedia's_W.svg"; uri: "https://en.wikipedia.org/w/index.php?search=" }
                        ListElement { name: "Search Youtube"; image: "yt_icon_mono_dark.svg"; colorize:"red"; uri: "https://www.youtube.com/results?search_query="}
                        ListElement { name: "Google Maps"; image: "Google_Maps_Logo.svg"; colorize: "#34A853"; uri: "https://www.google.com/maps/search/"}
                        ListElement { name: "Open in Gmail"; image: "Gmail_icon.svg"; colorize: "#EA4335"; uri: "https://mail.google.com/mail/u/0/?view=cm&body="}
                        ListElement { name: "Google Translate"; image: "g_translate-white.svg"; colorize: "#4285F4"; uri: "https://translate.google.com/?text="}
                        ListElement { name: "Search on Lexico"; image: "Lexico_logo.svg"; colorize: "#4FB46D"; uri: "https://www.lexico.com/definition/"}
                    }
                    ToolButton{
                        width: 140
                        Material.foreground: colorize ? colorize : systemPalette.text
                        icon.source: image
                        text: name
                        icon.width: 48
                        icon.height: 48
                        display: AbstractButton.TextUnderIcon
                        onClicked:{
                            Qt.openUrlExternally(uri+encodeURI(description))
                            Qt.quit()
                        }
                    }
                }
            }
        }
        ToolBar {
            id: bottomToolBar
            height:52
            width:parent.width
            Material.background: Material.background
            Material.elevation:0
            anchors.bottom:parent.bottom
            Button{
                flat:true
                anchors.rightMargin: 8
                anchors.right: parent.right
                Material.foreground: Material.accent
                text:"Close"
                font.pixelSize: 14
                onClicked:{
                    Qt.quit()
                }
            }
        }
    }
}
