#include <QtQuick/qquickview.h>
#include <QQmlApplicationEngine> 
#include <QGuiApplication>
#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <QtQuick/qquickitem.h>
#include <qqml.h>
#include <iostream>
#include <QClipboard>

using namespace std;
QQmlContext *context;

int main(int argc, char ** argv) {

    QGuiApplication app(argc, argv);
    
    app.setWindowIcon(QIcon::fromTheme("emblem-shared-symbolic"));
    string stringToShare;
    QQmlApplicationEngine view;
    context = view.rootContext();
    for (int i = 1; i < argc; ++i) {
        stringToShare.append(argv[i]);
        stringToShare.append(" ");
    }
    if(stringToShare != ""){
        view.rootContext()->setContextProperty("description", QString::fromStdString(stringToShare));
    }
    else{
        QClipboard *clipboard = QGuiApplication::clipboard();
        if(clipboard->supportsSelection())
            view.rootContext()->setContextProperty("description", clipboard->text(QClipboard::Selection));
        else
            view.rootContext()->setContextProperty("description", clipboard->text(QClipboard::Clipboard));
    }

    view.load("main.qml");
    
    return app.exec();
    
} 
